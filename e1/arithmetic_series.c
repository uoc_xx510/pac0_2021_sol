#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>

int main(int argc, char** argv) {
    uint32_t k, n, i;

    // Read the input values from the user
    printf("Enter k value: ");
    scanf("%d", &k);
    printf("Enter n value: ");
    scanf("%d", &n);
    printf("Enter i value: ");
    scanf("%d", &i);

    // Initialize the variables
    uint32_t result = 0;
    uint32_t scratch = k;

    // Compute the values and print the result
    printf("Serie: ");
    for (uint32_t x = 1; x <= i; x++) {
        printf("%d, ", scratch);
        result += scratch;
        scratch += n;
    }
    printf("\n");

    // Print the final report
    printf("k=%d, n=%d, i=%d, result=%d\n", k, n, i, result);

    return 0;
}
