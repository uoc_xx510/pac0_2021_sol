#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>

#define BUFFER_SIZE     ( 1024 )
#define LETTER_COUNT    ( 26 )

uint32_t count_letter(char letter, char* string, uint32_t length) {
    uint32_t result = 0;

    // Iterate over all the input string
    for (uint32_t i = 0; i < length; i++) {
        char scratch = string[i];

        // Check if input character is a letter, either capital or lowercase
        if ((scratch >= 'A' && scratch <= 'Z') || (scratch >= 'a' && scratch <= 'z')) {
            // If input letter is capital, convert to lowercase
            if (scratch >= 'A' && scratch <= 'Z') {
                scratch += 'a'-'A';
            }
            // If input letter is same as reference letter, count it
            if (scratch == letter) {
                result++;
            }
        }
    }

    return result;
}

int main(int argc, char** argv) {
    uint32_t letters_count = LETTER_COUNT;
    char letters_array[LETTER_COUNT];
    uint32_t letter_result[LETTER_COUNT];
    char buffer[BUFFER_SIZE];

    // Dinamically fill in the letters array with letters from 'a' to 'z' (lowercase)
    for (uint32_t i = 0; i < letters_count; i++) {
        letters_array[i] = 'a' + i; 
    }

    // Read the text from the input
    printf("Enter a text: ");
    scanf("%[^\n]", buffer);

    // Count length of the input buffer
    uint32_t buffer_length = strlen(buffer);

    // Iterate over all input buffer and count letters
    for (uint32_t i = 0; i < letters_count; i++) {
        char letter = letters_array[i];
        uint32_t result = count_letter(letter, buffer, buffer_length);
        letter_result[i] = result;
    }

    // Report the result
    for (uint32_t i = 0; i < letters_count; i++) {
        char letter = letters_array[i];
        uint32_t result = letter_result[i];
        printf("Letter=%c, Counter=%d \n", letter, result);
    }

    return 0;
}
