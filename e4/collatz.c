#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>

int main(int argc, char** argv) {
    uint32_t values[1024];
    uint32_t counter = 0;

    // Check if number of arguments is valid
    if (argc != 2) {
        printf("Error! Wrong number of parameters.\n");
        exit(-1);
    }

    // Convert argument to integer
    uint32_t variable = strtol(argv[1], NULL, 10);
    
    // Repeat until we reach 1 or counter reaches 1024
    while (variable != 1 && counter < 1024) {
        // Store variable in array
        values[counter] = variable;
        counter++;
        // Check if it is even
        if (variable % 2 == 0) {
            variable = variable / 2;
        } else { // Otherwise
            variable = 3 * variable + 1;
        }
    }

    // Print the result
    printf("Size: %d, Sequence: ", counter);
    for (uint32_t i = 0; i < counter; i++) {
        printf("%d, ", values[i]);
    }
    printf("1 \n");
       
    return 0;
}
