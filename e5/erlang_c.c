#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>

// Compute the factorial
uint32_t factorial(uint32_t f) { 
    uint32_t r = 1;
    while(f > 1) {
        r *= f;
        f--;
    }
    return r;
}

// Compute the power
float power(float b, uint32_t e) {
    float r = 1;
    for (uint32_t i = e; i > 0; i--) {
        r *= b;
    }
    return r;
}

// Compute the sum
float sum(uint32_t c, float r) {
    float result = 0;
    for (uint32_t n = 0; n <= c - 1; n++) {
        result += power(r,n) / factorial(n);
    }
    return result;
}

// Compute the quotient
float quotient(uint32_t c, float r) {
    float rho = r/c;
    uint32_t fact = factorial(c);
    float result = power(r, c) / (factorial(c) * (1 - rho));
    return result;
}

void main(int argc, char** argv) {
    // Check if number of arguments is valid
    if (argc != 3) {
        printf("Error! Wrong number of parameters.\n");
        exit(-1);
    }

    // Convert arguments to integer (number of servers) and float (offered load)
    uint32_t c = strtol(argv[1], NULL, 10);
    float    r = strtof(argv[2], NULL);

    // Check if number of servers is below offered load
    if ((float)c < r) {
        printf("Error! The number of servers is below the offered load.\n");
        exit(-1);
    }

    // Compute the quotient
    float scratch = quotient(c, r);

    // Compute the final result
    float result = scratch / (scratch + sum(c, r));

    // Print the result
    printf("Servers=%d, Load=%.2f, P_Waiting=%f.\n", c, r, result);
}
