#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>
#include <math.h>

// Compute the square root using the approximation
float square_root(float n, float k) {
    float result = 1.0;
    for (uint32_t i = 0; i < k; i++ ) {
        result = (result + n / result) / 2.0;
    }
    return result;
}

int main(int argc, char** argv) {
    
    // Check if number of arguments is valid
    if (argc != 3) {
        printf("Error! Wrong number of parameters.\n");
        exit(-1);
    }

    // Convert arguments 
    float variable = strtof(argv[1], NULL);
    float error_max = strtof(argv[2], NULL);

    // Compute the square root using the sqrtf function as a reference
    float r1 = sqrtf(variable);

    // Define variables to calculate the square root approximation
    float r2 = 0.0f;
    uint32_t iterations = 1;
    float error_now = 0.0f;
    
    // Iterate until the approximation is better than the defined threshold
    bool finish = false;
    do {
        // Compute the square root using the approximation
        r2 = square_root(variable, iterations);
        // Compute the error between the real value and the approximation
        error_now = fabs(r2 - r1);
        // Determine if the approximation is good enough
        finish = (error_now <= error_max);
        if (!finish) {
            iterations++;
        }
    } while(finish);

    // Report the result
    printf("r1 = %f, r2 = %f, max_error = %f, current_error = %f, iterations = %d\n", r1, r2, error_max, error_now, iterations);

    return 0;
}
